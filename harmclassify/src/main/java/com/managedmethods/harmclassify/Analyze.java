package com.managedmethods.harmclassify;

import com.google.appengine.api.utils.SystemProperty;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.annotation.WebServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.lang.RuntimeException;
import java.lang.Object;
import java.util.Scanner;

import edu.stanford.nlp.classify.Classifier;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.LinearClassifier;
import edu.stanford.nlp.ling.Datum;
import edu.stanford.nlp.objectbank.ObjectBank;
import edu.stanford.nlp.util.ErasureUtils;
import edu.stanford.nlp.util.Pair;

@WebServlet(name = "Analyze", value = "/analyze")
public class Analyze extends HttpServlet{

  private static String where = "";
  private static ColumnDataClassifier cdc;
  
  @Override
  public void init() {
    where = System.getProperty("user.dir") + File.separator;
    cdc = ColumnDataClassifier.getClassifier(where + "suicide-classify-model.ser.gz"); //risk vs norisk
	}
/*
classifyText will take in a string of text data and return the documents class according to that classifier
this classifier will either class text as 'risk' or 'norisk'
*/
  public static String classifyText(String txtdata)
  {
    String cls;
    String[] data = new String[2];
    System.out.println("ANALYZE");
    System.out.println(txtdata);
    data[0] = "none";
    data[1] = txtdata;
    Datum<String,String> d = cdc.makeDatumFromStrings(data);
    cls = cdc.classOf(d); 
    return cls;
  }
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
      	String text = request.getParameter("text").replace('\n', ' ');
      	String cls;
      	cls = classifyText(text);
      	System.out.println(text);
      	System.out.println(cls);
      	response.setContentType("text/html");
      	if(cls.equals("norisk")){
			request.setAttribute("cls", "No Risk");
			System.out.println("Found No Risk");
      	}
		else if(cls.equals("risk")){
			request.setAttribute("cls", "Risk");
			System.out.println("Yes, Risky");
		}
    	request.setAttribute("data",text);
    	request.getRequestDispatcher("view.jsp").forward(request, response);  
      }
}
