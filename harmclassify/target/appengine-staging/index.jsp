<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.managedmethods.harmclassify.HelloAppEngine" %>
<%@ page import="com.managedmethods.harmclassify.Analyze" %>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/stylesheet.css"/>
  <title>Self Harm Analysis</title>
</head>
<body style="text-align: center;">
    <h1>Self Harm Text Analysis</h1>
  <form method="POST" action="/analyze" id="form1">
      Enter Text to Analyze for Self Harm Risk<br>
      <textarea name="text" id ="textdata"></textarea><br>
      <input type="submit">
  </form>
  <div id="results">
  </div>
</body>
</html>
