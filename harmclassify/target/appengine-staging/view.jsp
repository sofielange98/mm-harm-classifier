<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.managedmethods.harmclassify.Analyze" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/stylesheet.css"/>
  <title>Self Harm Analysis</title>
</head>
<body style="text-align: center;">
    <h1>Self Harm Text Analysis</h1>
  <form method="POST" action="/analyze" id="form1">
      Enter Text to Analyze for Self Harm Risk<br>
      <textarea name="text" id ="textdata"></textarea><br>
      <input type="submit">
  </form>
  <div id="results">
      <table class="center" style="margin-top: 5px;">
        <tr>
          <th>
            Classified As :
          </th>
          <td>
            <c:out value="${cls}"/>
          </td>
        </tr>
        <tr>
          <th>
            Original String
          </th>
          <td>
            <c:out value="${data}"/>
          </td>
        </tr>
  </table>
  </div>
</body>
</html>
